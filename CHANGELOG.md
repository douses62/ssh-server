# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.3.16 - 2022-11-15(12:24:04 +0000)

### Other

- [SSH] Connection is dropped on IPv4 whenever there is an IPv6 change on same interface

## Release v0.3.15 - 2022-10-27(10:27:56 +0000)

### Other

- SSH server: regresssion:new baf handling incompatible with ifdef usage

## Release v0.3.14 - 2022-10-13(08:47:14 +0000)

### Other

- Improve plugin boot order

## Release v0.3.13 - 2022-10-13(08:02:49 +0000)

## Release v0.3.12 - 2022-09-27(12:09:35 +0000)

### Other

- [TR181-SSH] ssh instability when performing firstboots

## Release v0.3.11 - 2022-09-22(07:15:19 +0000)

### Other

- [amx][ssh] Too many restarts when Enabling an Ssh instance

## Release v0.3.10 - 2022-09-08(11:55:36 +0000)

### Other

- [amx][ssh-server] Remove the Vendor Extension X_PRPL-COM_ from the SSH root object.

## Release v0.3.9 - 2022-09-08(09:35:00 +0000)

### Other

- [SSHServer] Reduce log levels of some debug logs

## Release v0.3.8 - 2022-08-19(12:15:41 +0000)

### Fixes

- Call dropbear `hk_generate_as_needed` at boot

### Other

- Opensource component

## Release v0.3.7 - 2022-07-29(14:48:34 +0000)

### Fixes

- Wait for Firewall. before dynamically setting firewall rules

## Release v0.3.6 - 2022-06-30(08:24:29 +0000)

### Fixes

- [ssh-server] Remote SSH not working

## Release v0.3.5 - 2022-06-28(06:40:28 +0000)

### Other

- reduce log level of debug statements

## Release v0.3.4 - 2022-06-20(12:36:07 +0000)

### Other

- [PRPLoS] Reduce logging during start up.

## Release v0.3.3 - 2022-06-16(06:47:05 +0000)

### Changes

- Set ssh-server as default

## Release v0.3.2 - 2022-06-15(09:53:53 +0000)

### Fixes

- refused ssh connection after reboot

## Release v0.3.1 - 2022-05-19(12:38:31 +0000)

### Fixes

- [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when memory leaks are detected

## Release v0.3.0 - 2022-05-18(07:44:31 +0000)

### Other

- [amx][ssh-server] Integration of SSH Server plugin in prplOs

## Release v0.2.0 - 2022-05-03(13:05:06 +0000)

### Other

- [amx][SSH Server plugin] Integrate amx SSH server on SOP

## Release v0.1.0 - 2022-04-29(08:23:12 +0000)

### Fixes

- [tr181 plugins][makefile] Dangerous clean target for all tr181 components

### Other

- [amx][SSH Server plugin] Integrate amx SSH server on SOP

## Release 0.0.3 - 2021-03-01(18:12:08 +0000)

### Changes

- Update baf license
- Add copybara
- Updates readme - adds note about /proc/<pid>/task/<tid>/children and kernel versions

## Release 0.0.2 - 2021-02-28(19:41:05 +0000)

### New

- Unit tests

### Fixes

- Issues discovered with unit-test

## Release 0.0.1 - 2021-02-27(22:40:53 +0000)

### New

- Initial release
- launch and stop dropbear
- monitor dropbear child processes (sessions)
- data model dropbear configuration  parameters
- data model dropbear status parameters


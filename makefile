include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all
	$(MAKE) -C odl all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C odl clean

install: all
	$(INSTALL) -D -p -m 0644 odl/ssh_server.odl $(DEST)/etc/amx/ssh_server/ssh_server.odl
	$(INSTALL) -D -p -m 0644 odl/ssh_server_definition.odl $(DEST)/etc/amx/ssh_server/ssh_server_definition.odl
	$(INSTALL) -D -p -m 0644 odl/ssh_server_defaults.odl $(DEST)/etc/amx/ssh_server/ssh_server_defaults.odl
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/object/ssh-server.so $(DEST)/usr/lib/amx/ssh_server/ssh_server.so
	$(INSTALL) -d -m 0755 $(DEST)$(BINDIR)
	$(INSTALL) -D -p -m 0755 scripts/ssh-server.sh $(DEST)$(INITDIR)/$(COMPONENT)
	ln -sfr $(DEST)$(BINDIR)/amxrt $(DEST)$(BINDIR)/ssh_server

package: all
	$(INSTALL) -D -p -m 0644 odl/ssh_server.odl $(PKGDIR)/etc/amx/ssh_server/ssh_server.odl
	$(INSTALL) -D -p -m 0644 odl/ssh_server_definition.odl $(PKGDIR)/etc/amx/ssh_server/ssh_server_definition.odl
	$(INSTALL) -D -p -m 0644 odl/ssh_server_defaults.odl $(PKGDIR)/etc/amx/ssh_server/ssh_server_defaults.odl
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/object/ssh-server.so $(PKGDIR)/usr/lib/amx/ssh_server/ssh_server.so
	$(INSTALL) -d -m 0755 $(PKGDIR)$(BINDIR)
	$(INSTALL) -D -p -m 0755 scripts/ssh-server.sh $(PKGDIR)$(INITDIR)/$(COMPONENT)
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
	$(eval ODLFILES += odl/ssh_server.odl)
	$(eval ODLFILES += odl/ssh_server_definition.odl)
	$(eval ODLFILES += odl/ssh_server_defaults.odl)

	mkdir -p output/xml
	mkdir -p output/html
	mkdir -p output/confluence
	amxo-cg -Gxml,output/xml/$(COMPONENT).xml $(or $(ODLFILES), "")
	amxo-xml-to -x html -o output-dir=output/html -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml
	amxo-xml-to -x confluence -o output-dir=output/confluence -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml

test:
	$(MAKE) -C test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package doc test
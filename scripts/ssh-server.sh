#!/bin/sh

case $1 in
    start|boot)
        if [ ! -d /etc/config/ssh_server ]; then
            mkdir -p /etc/config/ssh_server/odl
        fi
        $(. /etc/init.d/dropbear && hk_generate_as_needed)
	ssh_server -D /etc/amx/ssh_server/ssh_server.odl
        ;;
    stop)
        if [ -f /var/run/ssh_server.pid ]; then
            kill `cat /var/run/ssh_server.pid`
        fi
        ;;
    debuginfo)
	echo "debuginfo for SSH server"
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    log)
	echo "TODO log ssh_server plugin"
	;;
    *)
        echo "Usage : $0 [start|boot|stop|debuginfo|log]"
        ;;
esac

/****************************************************************************
**
** Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "dm_ssh_server.h"

static int ssh_server_del_fw_service(const char* serviceid) {
    SAH_TRACEZ_INFO(ME, "ssh_server_del_fw_service: %s", serviceid);
    amxc_var_t ret;
    amxc_var_t args;
    int rv = 0;
    when_null(serviceid, exit);
    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "id", serviceid);
    rv = amxm_execute_function("fw", "fw", "delete_service", &args, &ret);
    if(rv != 0) {
        SAH_TRACEZ_WARNING(ME, "Firewall service %s could not be removed", serviceid);
    }
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
exit:
    return rv;
}

int ssh_server_disable_fw_services(amxd_object_t* server) {
    SAH_TRACEZ_INFO(ME, "ssh_server_disable_fw_services: %s",
                    amxd_object_get_name(server, AMXD_OBJECT_NAMED));
    int retval = -1;
    when_null(server, exit);
    ssh_server_instance_t* ssh_server = (ssh_server_instance_t*) server->priv;
    when_null(ssh_server, exit);
    if(ssh_server->ipv4_fw_serviceid) {
        ssh_server_del_fw_service(ssh_server->ipv4_fw_serviceid);
        free(ssh_server->ipv4_fw_serviceid);
        ssh_server->ipv4_fw_serviceid = NULL;
    }
    if(ssh_server->ipv6_fw_serviceid) {
        ssh_server_del_fw_service(ssh_server->ipv6_fw_serviceid);
        free(ssh_server->ipv6_fw_serviceid);
        ssh_server->ipv6_fw_serviceid = NULL;
    }
exit:
    return retval;
}

static const char* skip_prefix(const char* path) {
    const char* prefix = "Device.";

    if(strstr(path, prefix) == &path[0]) {
        path = &path[strlen(prefix)];
    }

    return path;
}

static char* ssh_server_get_alias_of_interface(const char* interface) {

    char* alias = NULL;
    when_null(interface, exit);
    if(strchr(interface, '.') != NULL) {
        amxd_path_t path;
        amxc_string_t alias_str;
        char* first = NULL;
        char* last = NULL;
        char* dot = NULL;

        interface = skip_prefix(interface);

        amxd_path_init(&path, interface);
        amxc_string_init(&alias_str, 0);

        first = amxd_path_get_first(&path, false);
        when_null_trace(first, exit, ERROR, "Failed to get first part of path[%s]", interface);

        last = amxd_path_get_last(&path, false);
        when_null_trace(last, exit2, ERROR, "Failed to get last part of path[%s]", interface);

        if((dot = strstr(first, ".")) != NULL) {
            *dot = '\0';
        }


        if((dot = strstr(last, ".")) != NULL) {
            *dot = '\0';
        }

        // E.g. Device.IP.Interface.2. -> alias = IP_2
        amxc_string_setf(&alias_str, "%s_%s", first, last);
        alias = amxc_string_take_buffer(&alias_str);

exit2:
        free(first);
        first = NULL;
        free(last);
        last = NULL;
        amxc_string_clean(&alias_str);
        amxd_path_clean(&path);
    } else {
        alias = strdup(interface);
    }
exit:
    return alias;
}

static int ssh_server_generate_fw_serviceid(char** serviceid,
                                            const char* interface,
                                            uint32_t port,
                                            uint32_t ipversion) {
    SAH_TRACEZ_INFO(ME, "generate serviceid: interface: %s port: %d ipversion: %d", interface, port, ipversion);
    int retval = -1;
    char* alias = NULL;
    int serviceid_len = 0;
    when_null(serviceid, exit);
    when_null(interface, exit);

    alias = ssh_server_get_alias_of_interface(interface);
    when_null_trace(alias, exit, ERROR, "Can not retrieve alias for interface %s", interface);
    /* service id format: ssh4_devname_port */
    serviceid_len = 3 + 1 + 1 + 10 + 1 + strlen(alias) + 1 + 5 + 1 + 5 + 1;
    *serviceid = calloc(1, serviceid_len);
    when_null(*serviceid, exit);
    snprintf(*serviceid, serviceid_len, "ssh%d_Interface_%s_Port_%d", ipversion, alias, port);
    retval = 0;
exit:
    free(alias);
    return retval;
}

static int ssh_server_add_fw_service(char** serviceid,
                                     const char* interface,
                                     uint32_t port,
                                     uint32_t ipversion,
                                     char* srcprefix) {
    SAH_TRACEZ_INFO(ME, "add fw service interface: %s port: %d ipversion: %d srcprefix:%s", interface, port, ipversion, srcprefix);

    amxc_var_t ret;
    amxc_var_t args;
    int retval = -1;

    when_null(serviceid, exit);
    when_null(interface, exit);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    retval = ssh_server_generate_fw_serviceid(serviceid,
                                              interface,
                                              port,
                                              ipversion);
    when_failed(retval, exit);

    amxc_var_add_key(cstring_t, &args, "id", *serviceid);
    amxc_var_add_key(cstring_t, &args, "interface", interface);
    amxc_var_add_key(uint32_t, &args, "destination_port", port);
    amxc_var_add_key(cstring_t, &args, "protocol", "6");
    amxc_var_add_key(uint32_t, &args, "ipversion", ipversion);
    amxc_var_add_key(cstring_t, &args, "source_prefix", srcprefix);
    amxc_var_add_key(bool, &args, "enable", true);

    retval = amxm_execute_function("fw", "fw", "set_service", &args, &ret);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Firewall failed to add service %s on interface %s", *serviceid, interface);
        free(*serviceid);
        *serviceid = NULL;
    } else {
        SAH_TRACEZ_INFO(ME, "Firewall service[%s] added", *serviceid);
    }
exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return retval;
}

int ssh_server_enable_fw_services(amxd_object_t* server) {
    SAH_TRACEZ_INFO(ME, "ssh_server_enable_fw_services: %s", amxd_object_get_name(server, AMXD_OBJECT_NAMED));
    uint8_t ipversion = 4;
    amxd_status_t status = amxd_status_ok;
    int retval = -1;
    uint32_t port = 0;
    char* intf = NULL;
    char* allowed_ipv6_srcprefix = NULL;
    char* allowed_ipv4_srcprefix = NULL;
    bool IPv6SourcePrefixNonEmpty = false;
    bool SourcePrefixNonEmpty = false;

    ssh_server_instance_t* ssh_server = NULL;
    when_null(server, exit);
    ssh_server = (ssh_server_instance_t*) server->priv;
    when_null(ssh_server, exit);
    intf = amxd_object_get_value(cstring_t, server, "Interface", NULL);
    port = amxd_object_get_value(uint32_t, server, "Port", NULL);
    allowed_ipv6_srcprefix = amxd_object_get_value(cstring_t, server, "Ipv6AllowedSourcePrefix", NULL);
    allowed_ipv4_srcprefix = amxd_object_get_value(cstring_t, server, "IPv4AllowedSourcePrefix", NULL);
    IPv6SourcePrefixNonEmpty = amxd_object_get_value(bool, server, "IPv6AllowEmptySourcePrefix", false);
    SourcePrefixNonEmpty = amxd_object_get_value(bool, server, "IPv4AllowEmptySourcePrefix", false);
    ipversion = amxd_object_get_value(uint8_t, server, "IPVersion", &status);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Can not fetch ipversion from datamodel");
        ipversion = 4;
    }
    if((ipversion == 0) || (ipversion == 4)) {
        if((!SourcePrefixNonEmpty) && (!allowed_ipv4_srcprefix || !*allowed_ipv4_srcprefix)) {
            SAH_TRACEZ_ERROR(ME, "Empty IPv4prefix not allowed");
            goto exit;
        }
    }
    if((ipversion == 0) || (ipversion == 6)) {
        if((!IPv6SourcePrefixNonEmpty) && (!allowed_ipv6_srcprefix || !*allowed_ipv6_srcprefix)) {
            SAH_TRACEZ_ERROR(ME, "Empty IPv6prefix not allowed");
            goto exit;
        }
    }
    retval = 0;
    if((ipversion == 0) || (ipversion == 4)) {
        retval = ssh_server_add_fw_service(&ssh_server->ipv4_fw_serviceid,
                                           intf,
                                           port,
                                           4,
                                           allowed_ipv4_srcprefix);
    }
    if((ipversion == 0) || (ipversion == 6)) {
        if(retval == 0) {
            retval = ssh_server_add_fw_service(&ssh_server->ipv6_fw_serviceid,
                                               intf,
                                               port,
                                               6,
                                               allowed_ipv6_srcprefix);
        }
    }
exit:
    free(intf);
    free(allowed_ipv6_srcprefix);
    free(allowed_ipv4_srcprefix);
    return retval;
}

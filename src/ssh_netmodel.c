/****************************************************************************
**
** Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <sys/stat.h>

#include <netmodel/client.h>
#include <netmodel/common_api.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "dm_ssh_server.h"

void ssh_server_close_netmodel_queries(ssh_server_instance_t* server) {
    SAH_TRACEZ_INFO(ME, "ssh_server close netmodel queries");
    when_null(server, exit);
    netmodel_closeQuery(server->ip_getaddrsquery);
    server->ip_getaddrsquery = NULL;
    amxc_string_clean(&server->ipaddr_str);
exit:
    return;
}

int ssh_server_open_netmodel_queries(ssh_server_instance_t* ssh_server,
                                     const char* intf,
                                     netmodel_callback_t ip_handler
                                     ) {
    uint8_t ipversion = 4;
    amxd_status_t status = amxd_status_ok;
    const char* query_expression = NULL;
    SAH_TRACEZ_INFO(ME, "ssh_server open netmodel queries");
    when_null(ssh_server, exit);
    when_null(ssh_server->object, exit);
    when_null(intf, exit);
    ipversion = amxd_object_get_value(uint8_t, ssh_server->object, "IPVersion", &status);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Can not fetch ipversion from datamodel");
        ipversion = 4;
    }
    switch(ipversion) {
    case 0:
        query_expression = "ipv4 || (ipv6 && global && !tentative && permanent)";
        break;
    case 4:
        query_expression = "ipv4";
        break;
    case 6:
        query_expression = "ipv6 && global && !tentative && permanent";
        break;
    default:
        query_expression = "ipv4";
        SAH_TRACEZ_ERROR(ME, "unknown version configured, defaults ipv4");
    }

    if(ssh_server->ip_getaddrsquery != NULL) {
        ssh_server_close_netmodel_queries(ssh_server);
    }
    ssh_server->ip_getaddrsquery = netmodel_openQuery_getAddrs(intf,
                                                               ME,
                                                               query_expression,
                                                               "down",
                                                               ip_handler,
                                                               ssh_server);
    when_null_trace(ssh_server->ip_getaddrsquery, error, ERROR, "ip query failure");
    return 0;
error:
    SAH_TRACEZ_ERROR(ME, "ssh_server queries failed on server instance: %s",
                     amxd_object_get_name(ssh_server->object, AMXD_OBJECT_NAMED));
    ssh_server_close_netmodel_queries(ssh_server);
exit:
    return -1;
}


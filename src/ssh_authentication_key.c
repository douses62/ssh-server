/****************************************************************************
**
** Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "dm_ssh_server.h"

#define AUTHORIZED_KEY "/etc/dropbear/authorized_keys"
static int add_server_authkeys_to_file(amxd_object_t* obj, FILE* keyfile) {
    int retval = -1;
    SAH_TRACEZ_INFO(ME, "add_server_authkeys_to_file for server: %s", amxd_object_get_name(obj, AMXD_OBJECT_NAMED));
    when_null(obj, exit);
    when_null(keyfile, exit);
    retval = 0;
    amxd_object_t* auth_obj = amxd_object_get_child(obj, "AuthorizedKey");
    when_null(auth_obj, exit);
    amxd_object_for_each(instance, it, auth_obj) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        char* key = amxd_object_get_value(cstring_t, instance, "Key", NULL);
        SAH_TRACEZ_INFO(ME, "authentication instance: %s  key: %s",
                        amxd_object_get_name(instance, AMXD_OBJECT_NAMED), key);
        if(key && *key) {
            retval = fwrite(key, strlen(key), 1, keyfile);
            free(key);
            if(retval != 1) {
                goto err;
            }
            retval = fwrite("\n", sizeof(char), 1, keyfile); /*new line between keys*/
            if(retval != 1) {
                goto err;
            }
            retval = 0;
            continue;
        } else {
            if(key) {
                free(key);
            }
        }
        continue;
err:
        SAH_TRACEZ_ERROR(ME, "server instance: %s authentication instance: %s unable to write to %s: %s", amxd_object_get_name(obj, AMXD_OBJECT_NAMED), amxd_object_get_name(instance, AMXD_OBJECT_NAMED), AUTHORIZED_KEY ".tmp", strerror(errno));
        retval = -1;
        break;
    }
exit:
    SAH_TRACEZ_INFO(ME, "retval authkey: %d", retval);
    return retval;
}

void ssh_handle_authorized_keys_changed(amxd_object_t* ssh_server_obj) {
    SAH_TRACEZ_INFO(ME, "ssh_server_authorized_keys_updated");
    when_null(ssh_server_obj, exit);
    /* WARNING: current implementation relies on dropbear: dropbear currently
       does not allow the use of different authorized key files on a running
       instance base. To overcome this current restriction and give a sense to a server
       instance based AuthorizedKey dm parameter it has been decided on short term to
       collect all the AuthorizedKey parameter values and store them all in the shared
       Authorized key file. This means keys are shared and not exclusively reserved for
       a running instance. This may change in the future if we decide to switch to another
       implementation or when the rootpath authorized path will be made configurable in
       opensource dropbear(TODO SAH?)
     */
    int ret = -1;
    FILE* keyfile = fopen(AUTHORIZED_KEY ".tmp", "w");
    when_null_trace(keyfile, exit, ERROR, "unable to open %s for writing: %s", AUTHORIZED_KEY ".tmp", strerror(errno));
    amxd_object_for_each(instance, it, ssh_server_obj) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        SAH_TRACEZ_INFO(ME, "instance name: %s", amxd_object_get_name(instance, AMXD_OBJECT_NAMED));
        ret = add_server_authkeys_to_file(instance, keyfile);
        if(ret) {
            break;
        }
    }
    fflush(keyfile);
    fclose(keyfile);
    if(!ret) {
        if(rename(AUTHORIZED_KEY ".tmp", AUTHORIZED_KEY) < 0) {
            SAH_TRACEZ_ERROR(ME, "unable to rename %s to %s: %s", AUTHORIZED_KEY ".tmp", AUTHORIZED_KEY, strerror(errno));
        }
        if(chmod(AUTHORIZED_KEY, S_IRUSR | S_IWUSR) < 0) {
            SAH_TRACEZ_ERROR(ME, "error changing mode of configuration file %s: %s", AUTHORIZED_KEY, strerror(errno));
        }
    }
    unlink(AUTHORIZED_KEY ".tmp");
exit:
    return;
}

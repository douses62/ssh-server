/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <sys/stat.h>

#include <netmodel/client.h>
#include <netmodel/common_api.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "dm_ssh_server.h"

static int ssh_server_initialize(amxd_object_t* templ, amxd_object_t* instance, void* priv);

static int ssh_server_start(UNUSED amxd_object_t* templ,
                            amxd_object_t* server,
                            UNUSED void* priv) {
    SAH_TRACEZ_INFO(ME, "ssh_server_start: %s", amxd_object_get_name(server, AMXD_OBJECT_NAMED));
    int retval = -1;
    uint32_t time = 0;
    char* status = NULL;
    ssh_server_instance_t* ssh_server = NULL;
    amxp_proc_ctrl_t* dropbear_ctrl = NULL;
    amxc_var_t settings;

    when_null(server, exit);
    /* enable firewall rules */
    retval = ssh_server_enable_fw_services(server);
    if(retval != 0) {
        ssh_server_disable_fw_services(server);
        ssh_server_update_status(server, (void*) "Error");
        return retval;
    }

    ssh_server = (ssh_server_instance_t*) server->priv;
    dropbear_ctrl = (amxp_proc_ctrl_t*) ssh_server->proc_ctrl;
    time = amxd_object_get_value(uint32_t, server, "ActivationDuration", NULL);
    status = amxd_object_get_value(cstring_t, server, "Status", NULL);
    when_null(status, exit);
    amxc_var_init(&settings);
    ssh_server_fetch_settings(server, &settings);
    SAH_TRACEZ_INFO(ME, "status: %s", status);
    if(strcmp(status, "Running") ||
       (!strcmp(status, "Running") && (ssh_server->proc_ctrl_stopping == true))) {
        if(ssh_server->proc_ctrl_stopping == false) {
            SAH_TRACEZ_INFO(ME, "start dropbear");
            retval = amxp_proc_ctrl_start(dropbear_ctrl, time, &settings);
            if(retval == 0) {
                ssh_server_update_status(server, (void*) "Running");
                ssh_server->pid = amxp_subproc_get_pid(dropbear_ctrl->proc);
                ssh_server_start_monitor();
            } else {
                ssh_server_disable_fw_services(server);
                ssh_server_update_status(server, (void*) "Error");
            }
        } else {
            ssh_server->proc_ctrl_restart = true;
        }
    }
    free(status);
    amxc_var_clean(&settings);
exit:
    return retval;
}

static int ssh_server_enable(UNUSED amxd_object_t* templ,
                             amxd_object_t* server,
                             UNUSED void* priv) {
    SAH_TRACEZ_INFO(ME, "ssh_server_enable");
    int retval = -1;
    char* intf = NULL;

    when_null(server, exit);
    ssh_server_update_status(server, (void*) "Enabled");
    ssh_server_instance_t* ssh_server = (ssh_server_instance_t*) server->priv;
    if(ssh_server) {
        intf = amxd_object_get_value(cstring_t, server, "Interface", NULL);
        when_str_empty(intf, error);
        retval = ssh_server_open_netmodel_queries(ssh_server, intf,
                                                  ip_getaddrs_cb);
        when_failed(retval, error);
        free(intf);
    } else {
        bool is_enabled = true;
        retval = ssh_server_initialize(NULL, server, &is_enabled);
        when_failed(retval, error);
    }
    return retval;
error:
    free(intf);
    retval = -1;
    ssh_server_update_status(server, (void*) "Error");
exit:
    return retval;
}

static int ssh_server_stop(UNUSED amxd_object_t* templ,
                           amxd_object_t* server,
                           UNUSED void* priv) {
    SAH_TRACEZ_INFO(ME, "ssh_server_stop");

    int retval = -1;
    const char* status = NULL;
    ssh_server_instance_t* ssh_server = NULL;

    when_null(server, exit);
    ssh_server = (ssh_server_instance_t*) server->priv;
    status = (const char*) priv;
    ssh_server_disable_fw_services(server);
    if(ssh_server) {
        ssh_server->proc_ctrl_restart = false;
        if(ssh_server->pid) {
            SAH_TRACEZ_INFO(ME, "ssh_server_stop ctrl");
            amxp_proc_ctrl_t* dropbear_ctrl = (amxp_proc_ctrl_t*) ssh_server->proc_ctrl;
            ssh_server->proc_ctrl_stopping = true;
            ssh_server->pid = amxp_subproc_get_pid(dropbear_ctrl->proc);
            retval = amxp_proc_ctrl_stop(dropbear_ctrl);
        } else {
            retval = 0;
        }
    }
    if((retval == 0) && status && *status) {
        ssh_server_update_status(server, (void*) status);
    }
exit:
    return retval;
}

static int ssh_server_disable(UNUSED amxd_object_t* templ,
                              amxd_object_t* server,
                              UNUSED void* priv) {
    SAH_TRACEZ_INFO(ME, "ssh server disable");

    int retval = -1;
    const char* status = NULL;
    ssh_server_instance_t* ssh_server = NULL;
    when_null(server, exit);
    status = (const char*) priv;
    ssh_server = (ssh_server_instance_t*) server->priv;

    if(ssh_server) {
        ssh_server_close_netmodel_queries(ssh_server);
        retval = ssh_server_stop(NULL, server, NULL);
    }
    if((retval == 0) && status && *status) {
        ssh_server_update_status(server, status);
    }
exit:
    return retval;
}

static void ssh_server_proc_disable(UNUSED const char* const event_name,
                                    const amxc_var_t* const event_data,
                                    UNUSED void* const priv) {

    /* proc disable received after Activation Timeout */
    SAH_TRACEZ_INFO(ME, "proc disable");
    uint32_t pid = 0;
    amxd_dm_t* dm = NULL;
    amxd_object_t* server = NULL;
    const char* status = "Disabled";

    when_null(event_data, exit);
    pid = amxc_var_dyncast(uint32_t, event_data);
    dm = ssh_server_get_dm();
    SAH_TRACEZ_INFO(ME, "proc disabled: %d", pid);
    server = amxd_dm_findf(dm, "%sSSH.Server.[ PID == %d ].", ssh_server_get_vendor_prefix(), pid);
    if(server != NULL) {
        ssh_server_update_status(server, (void*) status);
        ssh_server_instance_t* ssh_server = (ssh_server_instance_t*) server->priv;
        ssh_server->proc_ctrl_stopping = false;
        ssh_server->proc_ctrl_restart = false;
        ssh_server->pid = 0;
    }
exit:
    return;
}

static void ssh_server_proc_stopped(UNUSED const char* const event_name,
                                    const amxc_var_t* const event_data,
                                    UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "proc stopped");
    uint32_t pid = 0;
    amxd_dm_t* dm = NULL;
    amxd_object_t* server = NULL;
    const char* status = "Stopped";

    when_null(event_data, exit);
    pid = amxc_var_dyncast(uint32_t, event_data);
    dm = ssh_server_get_dm();
    SAH_TRACEZ_INFO(ME, "Dropbear instance stopped: %d", pid);
    server = amxd_dm_findf(dm, "%sSSH.Server.", ssh_server_get_vendor_prefix());
    amxd_object_for_each(instance, child_it, server) {
        amxd_object_t* instance_obj = amxc_llist_it_get_data(child_it, amxd_object_t, it);
        ssh_server_instance_t* ssh_server = (ssh_server_instance_t*) instance_obj->priv;
        if(ssh_server && (ssh_server->pid == pid)) {
            SAH_TRACEZ_INFO(ME, "Running Instance");
            ssh_server->proc_ctrl_stopping = false;
            ssh_server->pid = 0;
            if(!ssh_server->proc_ctrl_restart) {
                ssh_server_update_status(instance_obj, (void*) status);
            } else {
                SAH_TRACEZ_INFO(ME, "restart dropbear for server");
                amxc_var_t settings;
                amxp_proc_ctrl_t* dropbear_ctrl = (amxp_proc_ctrl_t*) ssh_server->proc_ctrl;
                uint32_t time = 0;
                ssh_server->proc_ctrl_restart = false;
                int retval = 0;

                amxc_var_init(&settings);
                ssh_server_fetch_settings(instance_obj, &settings);
                time = amxd_object_get_value(uint32_t, instance_obj, "ActivationDuration", NULL);
                retval = amxp_proc_ctrl_start(dropbear_ctrl, time, &settings);
                if(retval == 0) {
                    ssh_server_update_status(instance_obj, (void*) "Running");
                    ssh_server->pid = amxp_subproc_get_pid(dropbear_ctrl->proc);
                    ssh_server_start_monitor();
                } else {
                    ssh_server_disable_fw_services(instance_obj);
                    ssh_server_update_status(instance_obj, (void*) "Error");
                }
                amxc_var_clean(&settings);
            }
        }
    }
exit:
    return;
}

void ip_getaddrs_cb(UNUSED const char* sig_name,
                    const amxc_var_t* data,
                    void* priv) {

    bool changed = false;
    SAH_TRACEZ_INFO(ME, "ip_getaddrs_cb");
    ssh_server_instance_t* server = NULL;

    when_null(data, exit);
    when_null(priv, exit);
    server = (ssh_server_instance_t*) priv;
    {
        const amxc_llist_t* list = amxc_var_constcast(amxc_llist_t, data);
        bool first = true;
        amxc_string_t ipaddr_str;
        amxc_string_init(&ipaddr_str, 0);

        if(amxc_llist_is_empty(list)) {
            if(!amxc_string_is_empty(&server->ipaddr_str)) {
                changed = true;
            }
        } else {
            amxc_llist_for_each(it, list) {
                amxc_var_t* addrvar = amxc_var_from_llist_it(it);
                const char* ipaddr = GET_CHAR(addrvar, "Address");
                if(first) {
                    amxc_string_append(&ipaddr_str, ipaddr, strlen(ipaddr));
                    first = false;
                } else {
                    amxc_string_appendf(&ipaddr_str, ",%s", ipaddr);
                }
                SAH_TRACEZ_INFO(ME, "ipstr: %s", ipaddr_str.buffer);
            }
            SAH_TRACEZ_INFO(ME, "old ipaddrs: %s new ipaddrs: %s",
                            server->ipaddr_str.buffer,
                            ipaddr_str.buffer);
            if((amxc_string_is_empty(&server->ipaddr_str) &&
                !amxc_string_is_empty(&ipaddr_str)) ||
               (!amxc_string_is_empty(&server->ipaddr_str) &&
                amxc_string_is_empty(&ipaddr_str)) ||
               (strcmp(ipaddr_str.buffer, server->ipaddr_str.buffer))) {
                SAH_TRACEZ_INFO(ME, "ip addrs changed");
                changed = true;
            }
        }
        if(!changed) {
            goto exit2;
        }
        if(!amxc_string_is_empty(&server->ipaddr_str)) {
            ssh_server_stop(NULL, server->object, NULL);
            amxc_string_clean(&server->ipaddr_str);
        }
        if(!amxc_string_is_empty(&ipaddr_str)) {
            amxc_string_copy(&server->ipaddr_str, &ipaddr_str);
            ssh_server_start(NULL, server->object, NULL);
        }
        SAH_TRACEZ_INFO(ME, "server ipstr: %s", server->ipaddr_str.buffer);
exit2:
        amxc_string_clean(&ipaddr_str);
    }
exit:
    return;
}

static int ssh_server_create(ssh_server_instance_t** ssh_server,
                             amxd_object_t* instance) {
    SAH_TRACEZ_INFO(ME, "ssh_server_create");
    int retval = -1;
    *ssh_server = calloc(1, sizeof(ssh_server_instance_t));
    when_null(*ssh_server, exit);
    amxc_string_init(&(*ssh_server)->ipaddr_str, 0);
    (*ssh_server)->object = instance;
    instance->priv = *ssh_server;
    retval = amxp_proc_ctrl_new(&(*ssh_server)->proc_ctrl, dropbear_ctrl_build_cmd);
    when_failed_trace(retval, exit, ERROR, "Can not create server control");
exit:
    return retval;
}

int ssh_server_initialize(UNUSED amxd_object_t* templ,
                          amxd_object_t* instance,
                          void* priv) {
    SAH_TRACEZ_INFO(ME, "ssh_server_initialize instance:%s",
                    amxd_object_get_name(instance, AMXD_OBJECT_NAMED));
    int retval = -1;
    bool* is_enabled = NULL;
    bool instance_enabled = false;
    ssh_server_instance_t* ssh_server = NULL;

    when_null(instance, exit1);
    when_null(priv, exit1);

    if(instance->priv == NULL) {
        retval = ssh_server_create(&ssh_server, instance);
        when_failed(retval, error);
    } else {
        ssh_server = instance->priv;
    }
    is_enabled = (bool*) priv;
    instance_enabled = amxd_object_get_value(bool, instance, "Enable", NULL);
    retval = 0;
    if(*is_enabled && instance_enabled) {
        SAH_TRACEZ_INFO(ME, "enable instance");
        char* intf = amxd_object_get_value(cstring_t, instance,
                                           "Interface", NULL);
        when_str_empty_trace(intf, exit2, ERROR,
                             "try to enable ssh instance %s with empty interface",
                             amxd_object_get_name(instance, AMXD_OBJECT_NAMED));
        SAH_TRACEZ_INFO(ME, "open queries on intf: %s", intf);
        ssh_server_update_status(instance, (void*) "Enabled");
        retval = ssh_server_open_netmodel_queries(ssh_server, intf,
                                                  ip_getaddrs_cb);
exit2:
        free(intf);
    }
    if(retval == 0) {
        return retval;
    }
error:
    ssh_server_close_netmodel_queries(ssh_server);
    ssh_server_update_status(instance, (void*) "Error");
exit1:
    return retval;
}

void _app_start(UNUSED const char* const event_name,
                UNUSED const amxc_var_t* const event_data,
                UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "app_start");
    amxd_dm_t* dm = ssh_server_get_dm();
    amxd_object_t* ssh = amxd_dm_findf(dm, "%sSSH", ssh_server_get_vendor_prefix());
    bool is_enabled = amxd_object_get_value(bool, ssh, "Enable", NULL);
    bool start = true;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxp_sigmngr_add_signal(NULL, "proc:disable");
    amxp_sigmngr_add_signal(NULL, "proc:stopped");
    amxp_slot_connect(NULL, "proc:disable", NULL, ssh_server_proc_disable, NULL);
    amxp_slot_connect(NULL, "proc:stopped", NULL, ssh_server_proc_stopped, NULL);
    _ssh_server_authorized_keys_changed(NULL, NULL, &start);
    amxd_object_t* servers = amxd_object_get_child(ssh, "Server");
    amxd_object_for_all(servers, "*", ssh_server_initialize, &is_enabled);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void _ssh_toggle(UNUSED const char* const event_name,
                 const amxc_var_t* const event_data,
                 UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "ssh_toogle");
    amxd_object_t* ssh = NULL;

    when_null(event_data, exit);
    ssh = amxd_dm_signal_get_object(ssh_server_get_dm(), event_data);
    if(ssh) {
        bool enable = GETP_BOOL(event_data, "parameters.Enable.to");
        amxd_instance_cb_t fn = ssh_server_stop;
        const char* filter = "[Status != 'Stopped'].";
        const char* status = "Stopped";
        amxd_object_t* servers = NULL;
        if(enable) {
            fn = ssh_server_enable;
            filter = "[Enable == true].";
            status = NULL;
            ssh_update_status(ssh, "Enabled");
        } else {
            ssh_update_status(ssh, "Disabled");
        }
        servers = amxd_object_get_child(ssh, "Server");
        amxd_object_for_all(servers, filter, fn, (void*) status);
    }
exit:
    return;
}

void _ssh_server_added(UNUSED const char* const event_name,
                       const amxc_var_t* const event_data,
                       UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "_ssh_server_added");
    amxd_object_t* ssh_servers = NULL;

    when_null(event_data, exit);
    ssh_servers = amxd_dm_signal_get_object(ssh_server_get_dm(), event_data);
    if(ssh_servers) {
        amxd_object_t* ssh = amxd_object_get_parent(ssh_servers);
        amxd_object_t* server = NULL;
        uint32_t index = GET_UINT32(event_data, "index");
        bool is_enabled = amxd_object_get_value(bool, ssh, "Enable", NULL);
        server = amxd_object_get_instance(ssh_servers, NULL, index);
        if(server != NULL) {
            ssh_server_instance_t* ssh_server = (ssh_server_instance_t*) server->priv;
            if(!ssh_server) {
                ssh_server_initialize(ssh_servers, server, &is_enabled);
            }
        }
    }
exit:
    return;
}

void _ssh_server_enable_changed(UNUSED const char* const event_name,
                                const amxc_var_t* const event_data,
                                UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "ssh_server_enable_changed");
    amxd_object_t* server = NULL;

    when_null(event_data, exit);
    server = amxd_dm_signal_get_object(ssh_server_get_dm(), event_data);
    if(server) {
        amxd_object_t* ssh_servers = amxd_object_get_parent(server);
        amxd_object_t* ssh = amxd_object_get_parent(ssh_servers);
        bool server_is_enabled = GETP_BOOL(event_data, "parameters.Enable.to");
        bool ssh_is_enabled = amxd_object_get_value(bool, ssh, "Enable", NULL);
        if(ssh_is_enabled) {
            if(server_is_enabled) {
                SAH_TRACEZ_INFO(ME, "server enable");
                ssh_server_enable(NULL, server, NULL);
            } else {
                SAH_TRACEZ_INFO(ME, "server disable");
                ssh_server_disable(NULL, server, (void*) "Disabled");
            }
        }
    }
exit:
    return;
}

void _ssh_server_duration_changed(UNUSED const char* const event_name,
                                  const amxc_var_t* const event_data,
                                  UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "ssh_server_duration_changed");
    amxd_object_t* server = NULL;
    when_null(event_data, exit);
    server = amxd_dm_signal_get_object(ssh_server_get_dm(), event_data);
    if(server) {
        uint32_t time = GETP_UINT32(event_data, "parameters.ActivationDuration.to");
        ssh_server_instance_t* ssh_server = (ssh_server_instance_t*) server->priv;
        SAH_TRACEZ_INFO(ME, "ssh_server_duration_changed: duration: %d", time);
        if(ssh_server) {
            amxp_proc_ctrl_t* dropbear_ctrl = ssh_server->proc_ctrl;
            if(dropbear_ctrl) {
                amxp_proc_ctrl_set_active_duration(dropbear_ctrl, time);
            }
        }
    }
exit:
    return;
}
void _ssh_server_interface_changed(UNUSED const char* const event_name,
                                   const amxc_var_t* const event_data,
                                   UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "ssh_server_interface_changed");
    amxc_var_t* config = NULL;

    when_null(event_data, exit);
    config = ssh_server_get_config();
    if(GETP_BOOL(config, "ssh-server.auto-restart")) {
        amxd_object_t* server = amxd_dm_signal_get_object(ssh_server_get_dm(), event_data);
        if(server) {
            amxd_object_t* ssh_servers = amxd_object_get_parent(server);
            amxd_object_t* ssh = amxd_object_get_parent(ssh_servers);
            bool ssh_is_enabled = amxd_object_get_value(bool, ssh, "Enable", NULL);
            bool server_enable = amxd_object_get_value(bool, server, "Enable", NULL);
            if(ssh_is_enabled) {
                if(server_enable) {
                    SAH_TRACEZ_INFO(ME, "server enable");
                    char* status = amxd_object_get_value(cstring_t, server, "Status", NULL);
                    if(strcmp(status, "Disabled")) {
                        ssh_server_disable(NULL, server, NULL);
                    }
                    ssh_server_enable(NULL, server, NULL);
                    free(status);
                }
            }
        }
    }
exit:
    return;
}

void _ssh_server_settings_changed(UNUSED const char* const event_name,
                                  const amxc_var_t* const event_data,
                                  UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "ssh_server_settings_changed");
    amxc_var_t* config = NULL;

    when_null(event_data, exit);
    config = ssh_server_get_config();
    if(GETP_BOOL(config, "ssh-server.auto-restart")) {
        amxd_object_t* server = amxd_dm_signal_get_object(ssh_server_get_dm(), event_data);
        if(server != NULL) {
            amxd_object_t* ssh_servers = amxd_object_get_parent(server);
            amxd_object_t* ssh = amxd_object_get_parent(ssh_servers);
            bool ssh_is_enabled = amxd_object_get_value(bool, ssh, "Enable", NULL);
            char* status = amxd_object_get_value(cstring_t, server, "Status", NULL);
            bool server_enable = amxd_object_get_value(bool, server, "Enable", NULL);
            if(ssh_is_enabled) {
                if(server_enable) {
                    SAH_TRACEZ_INFO(ME, "server enable");
                    if((strcmp(status, "Running") == 0)) {
                        ssh_server_stop(NULL, server, NULL);
                        ssh_server_start(NULL, server, NULL);
                    } else {
                        if(strcmp(status, "Disabled")) {
                            ssh_server_disable(NULL, server, NULL);
                        }
                        ssh_server_enable(NULL, server, NULL);
                    }
                }
            }
            free(status);
        }
    }
exit:
    return;
}

void _ssh_server_authorized_keys_changed(UNUSED const char* const event_name,
                                         UNUSED const amxc_var_t* const event_data,
                                         void* const priv) {
    SAH_TRACEZ_INFO(ME, "ssh_server_authorized_keys_updated");
    const bool* start = (const bool*) priv;
    amxd_object_t* root = amxd_dm_get_root(ssh_server_get_dm());
    amxd_object_t* obj = NULL;

    obj = amxd_object_findf(root, "%sSSH.Server.", ssh_server_get_vendor_prefix());
    when_null(obj, exit);
    if((start == NULL) || (*start == false)) {
        amxd_object_for_all(obj, "[Status != 'Disabled'].", ssh_server_disable, NULL);
    }
    ssh_handle_authorized_keys_changed(obj);
    if((start == NULL) || (*start == false)) {
        amxd_object_for_all(obj, "[Enable == true].", ssh_server_enable, NULL);
    }
exit:
    return;
}
